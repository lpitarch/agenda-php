<?php
	class pagina{
		public function encabezado($title=""){
			$estilo1="css/estilo1.css";
			echo "<!DOCTYPE html>
	
	<head>
	    <meta charset='utf-8'>
	    <title>$title</title>
		<link href='".$estilo1."' type='text/css' rel='stylesheet'>       
	</head>
	<body>
	";
		}
		
		public function contenido(){
			echo "<div class='contenido'>";
			include("inc/cabecera.php"); 
			include("inc/cuerpo.php"); 
			include("inc/pie.php");
			
		}
		
		public function htmlfin(){
			echo "</div></body></html>";
		}
		
	}
	//compruebo que exista la base de datos
		if (!file_exists("contactos.db")){
			header("Location: index.php");//sino redirijo al inicio para crearla		
		}
	$element_menu=array("Inicio","Añadir","Listar","Modificar","Borrar","Borrar todo","Buscar");
	// creo la pagina 
	$p = new pagina();
	
	// creo el head con el titulo
	$p->encabezado("Agenda : Borrar");
	
	// empiezo a añadir contenido
	$p->contenido();
	
	// creo la imagen y el menu de la cabecera
	$cab = new cabecera();
	$cab->banner();
	$cab->menu($element_menu);
	
	
	$cue = new cuerpo();
	$cue->title_borrar_contacto();
	include("inc/sql.php");
	$sql = new modelos();
	
	if(isset($_POST['borrar'])&&isset($_POST['id'])){//si tengo id para borrar usuarios y han pulsado el boton borrar
		
		foreach ($_POST['id'] as $key => $id) {// creo un bucle para borrar los indicados
			$msg=$sql->borrar_contacto($id);
		}
		if($msg==true){
			if (count($_POST['id'])>1){
				echo "<h4>".count($_POST['id'])." contactos borrados con éxito</h4>";
			}else{
				echo "<h4>Contacto borrado con éxito</h4>";
				
			}
			?>
			<div class="mensajes">
				<br>
				<a href='borrar.php'>Ok, quiero borrar más</a>
				<a href='index.php'>Ok, suficiente</a>
			</div>
			<?php
		}else{
			echo "<h4>Error al eliminar</h4>";
			?>
			<div class="mensajes">
				<br>
				<a href='borrar.php'>Intentalo otra vez</a>
				<a href='index.php'>Me doy por vencido...</a>
			</div>
			<?php
		}
		
	}else{// Si no tengo contactos seleccionados, muestro la lista
		$datos=$sql->listar();
		if ($datos!=null){
			if((isset($_GET['c'])) && (isset($_GET['o']))){
				$campo=$_GET['c'];
				$orden=$_GET['o'];
				//vuelvo a pedir la consulta pero ahora ordenada
				$datos=$sql->listar($campo,$orden);
			}
			$cue->mostrar_datos($datos,"borrar");
			if(isset($_POST['borrar'])&&!isset($_POST['id'])){
				echo "<h3>Selecciona un contacto borrar</h3>";
			}
		}else{//o si no hay contactos
			echo "<h2>Tu lista de contactos está vacia :(</h2>";
		}
	}
	$pie = new pie();
	$pie->mostrarPie();
	// cierro el contenedor y el html
	$p->htmlfin();
?>