<?php
	class pagina{
		public function encabezado($title=""){
			$estilo1="css/estilo1.css";
			echo "<!DOCTYPE html>
	
	<head>
	    <meta charset='utf-8'>
	    <title>$title</title>
		<link href='".$estilo1."' type='text/css' rel='stylesheet'>       
	</head>
	<body>
	";
		}
		
		public function contenido(){
			echo "<div class='contenido'>";
			include("inc/cabecera.php"); 
			include("inc/cuerpo.php"); 
			include("inc/pie.php");
			
		}
		
		public function htmlfin(){
			echo "</div></body></html>";
		}
		
	}
	//compruebo que exista la base de datos
		if (!file_exists("contactos.db")){
			header("Location: index.php");//sino redirijo al inicio para crearla		
		}
	$element_menu=array("Inicio","Añadir","Listar","Modificar","Borrar","Borrar todo","Buscar");
	// creo la pagina principal
	$p = new pagina();
	
	// creo el head con el titulo
	$p->encabezado("Agenda : Añadir");
	
	// empiezo a añadir contenido
	$p->contenido();
	
	// creo la imagen y el menu de la cabecera
	$cab = new cabecera();
	$cab->banner();
	$cab->menu($element_menu);
	
	
	$cue = new cuerpo();
	$cue->title_añadir();
	
	$cue->formulario_añadir("añadir");
	
	
	if(isset($_POST['enviar'])){// si he pulsado en añadir contacto
		if(isset($_POST['nombre'])&&isset($_POST['telefono'])&&$_POST['nombre']!=""&&$_POST['telefono']!=""){
			//compruebo que haya un nombre y un telefono que añadir
			include("inc/sql.php");
			$sql = new modelos();
			$existe=$sql->buscar_nombre($_POST['nombre']);// busco si el nombre ya existe, devuelve true si no existe
			if($existe){//si no existe, puedo añadirlo
				if(is_numeric($_POST['telefono'])){	
					// si el teléfono es numérico
					$msg=$sql->insertar_datos($_POST,"añadir");//ejecuto la operación
					if($msg==1){
						echo "<h3>Éxito al añadir</h3>";
						
					}else{
						echo "<h3>Error al añadir</h3>";
					}
				}else{
					echo "<h3>El número de teléfono sólo debe contener dígitos</h3>";
				}
			}else{
				echo "<h3>Ese contacto ya existe, introduce otro nombre</h3>";
			}
		}else{
			echo "<h3>Introduce al menos el nombre y el teléfono</h3>";
		}
	}
	
	$pie = new pie();
	$pie->mostrarPie();
	// cierro el contenedor y el html
	$p->htmlfin();
?>