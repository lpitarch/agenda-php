<?php
	class pagina{
		public function encabezado($title=""){
			$estilo1="css/estilo1.css";
			echo "<!DOCTYPE html>

    <head>
        <meta charset='utf-8'>
        <title> $title</title>
 		<link href='".$estilo1."' type='text/css' rel='stylesheet'>       
    </head>
    <body>
    ";
		}
		
		public function contenido(){
			echo "<div class='contenido'>";
			include("inc/cabecera.php"); 
			include("inc/cuerpo.php"); 
			include("inc/pie.php");
			
		}
		
		public function htmlfin(){
			echo "</div></body></html>";
		}
		
	}
	$element_menu=array("Inicio","Añadir","Listar","Modificar","Borrar","Borrar todo","Buscar");
	// creo la pagina principal
	$p = new pagina();
	
	// creo el head con el titulo
	$p->encabezado("Agenda : Inicio");
	
	// empiezo a añadir contenido
	$p->contenido();
	
	// creo la imagen y el menu de la cabecera
	$cab = new cabecera();
	$cab->banner();
	$cab->menu($element_menu);
	
	
	$cue = new cuerpo();
	$cue->title_inicio();//pongo un título
	$cue->portada(); //saco la imagen de la portada
	
	include("inc/sql.php");
	$sql = new modelos();
	$sql->crear_tabla("contactos");//creo la base de datos y la tabla si no existen
	
	$pie = new pie();
	$pie->mostrarPie();
	// cierro el contenedor y el html
	$p->htmlfin();
?>

    	
    