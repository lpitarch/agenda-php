<?php
	class pagina{
		public function encabezado($title=""){
			$estilo1="css/estilo1.css";
			echo "<!DOCTYPE html>
	
	<head>
	    <meta charset='utf-8'>
	    <title>$title</title>
		<link href='".$estilo1."' type='text/css' rel='stylesheet'>       
	</head>
	<body>
	";
		}
		
		public function contenido(){
			echo "<div class='contenido'>";
			include("inc/cabecera.php"); 
			include("inc/cuerpo.php"); 
			include("inc/pie.php");
			
		}
		
		public function htmlfin(){
			echo "</div></body></html>";
		}
		
	}
	//compruebo que exista la base de datos
	if (!file_exists("contactos.db")){
		header("Location: index.php");//sino redirijo al inicio para crearla		
	}
	$element_menu=array("Inicio","Añadir","Listar","Modificar","Borrar","Borrar todo","Buscar");
	// creo la pagina
	$p = new pagina();
	
	// creo el head con el titulo
	$p->encabezado("Agenda : Buscar");
	
	// empiezo a añadir contenido
	$p->contenido();
	
	// creo la imagen y el menu de la cabecera
	$cab = new cabecera();
	$cab->banner();
	$cab->menu($element_menu);
	
	
	$cue = new cuerpo();
	$cue->title_buscar();
	
	
	
	if(isset($_GET['campo'])&&isset($_GET['criterio'])){//si recibo por get las preferencias de busqueda
		$campo= $_GET['campo'];
		$criterio= $_GET['criterio'];
		include("inc/sql.php");
		$sql = new modelos();
		
		$datos=$sql->buscar($campo,$criterio); //las busco
		
		if ($datos!=null){// si encuentro algo
			if((isset($_GET['c'])) && (isset($_GET['o']))){// y si recibo preferencias de ordenacion
				
				$columna=$_GET['c'];
				$orden=$_GET['o'];
				//vuelvo a pedir la consulta pero ahora ordenada
				
				$datos=$sql->buscar($campo,$criterio,$columna,$orden);// ordeno la búsqueda				
			}
		$cue->mostrar_datos($datos);//muestro el resultado
		}else{
			echo "<h2>No se encontraron contactos con los requisitos indicados :(</h2>";
		}
	
	}else{
		$cue->form_buscar();
	}
	
	$pie = new pie();
	$pie->mostrarPie();
	// cierro el contenedor y el html
	$p->htmlfin();
?>