<?php
	class pagina{
		public function encabezado($title=""){
			$estilo1="css/estilo1.css";
			echo "<!DOCTYPE html>

    <head>
        <meta charset='utf-8'>
        <title>$title</title>
 		<link href='".$estilo1."' type='text/css' rel='stylesheet'>       
    </head>
    <body>
    ";
		}
		
		public function contenido(){
			echo "<div class='contenido'>";
			include("inc/cabecera.php"); 
			include("inc/cuerpo.php"); 
			include("inc/pie.php");
			
		}
		
		public function htmlfin(){
			echo "</div></body></html>";
		}
		
	}
	//compruebo que exista la base de datos
	if (!file_exists("contactos.db")){
		header("Location: index.php");//sino redirijo al inicio para crearla		
	}
	$element_menu=array("Inicio","Añadir","Listar","Modificar","Borrar","Borrar todo","Buscar");
	// creo la pagina
	$p = new pagina();
	
	// creo el head con el titulo
	$p->encabezado("Agenda : Listado");
	
	// empiezo a añadir contenido
	$p->contenido();
	
	// creo la imagen y el menu de la cabecera
	$cab = new cabecera();
	$cab->banner();
	$cab->menu($element_menu);
	
	
	$cue = new cuerpo();
	$cue->title_listar();
	
	include("inc/sql.php");
	$sql = new modelos();
	$datos=$sql->listar();
	
	if ($datos!=null){
		if((isset($_GET['c'])) && (isset($_GET['o']))){// si quiero ver la lista ordenada
			$campo=$_GET['c'];
			$orden=$_GET['o'];
			//vuelvo a pedir la consulta pero ahora ordenada
			$datos=$sql->listar($campo,$orden);
		}
		$cue->mostrar_datos($datos);
	}else{
		echo "<h2>Tu lista de contactos está vacia :(</h2>";
	}
	
	$pie = new pie();
	$pie->mostrarPie();
	// cierro el contenedor y el html
	$p->htmlfin();
?>