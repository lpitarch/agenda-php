<?php
	class pagina{
		public function encabezado($title=""){
			$estilo1="css/estilo1.css";
			echo "<!DOCTYPE html>
	
	<head>
	    <meta charset='utf-8'>
	    <title>$title</title>
		<link href='".$estilo1."' type='text/css' rel='stylesheet'>       
	</head>
	<body>
	";
		}
		
		public function contenido(){
			echo "<div class='contenido'>";
			include("inc/cabecera.php"); 
			include("inc/cuerpo.php"); 
			include("inc/pie.php");
			
		}
		
		public function htmlfin(){
			echo "</div></body></html>";
		}
		
	}
	//compruebo que exista la base de datos
		if (!file_exists("contactos.db")){
			header("Location: index.php");//sino redirijo al inicio para crearla		
		}
	$element_menu=array("Inicio","Añadir","Listar","Modificar","Borrar","Borrar todo","Buscar");
	// creo la pagina principal
	$p = new pagina();
	
	// creo el head con el titulo
	$p->encabezado("Agenda : Borrar");
	
	// empiezo a añadir contenido
	$p->contenido();
	
	// creo la imagen y el menu de la cabecera
	$cab = new cabecera();
	$cab->banner();
	$cab->menu($element_menu);
	
	
	$cue = new cuerpo();
	$cue->title_borrar();
	
	
	
	if(isset($_POST['borrar'])){// si han pulsado el boton de borrar
		include("inc/sql.php");
		$sql = new modelos();
		$msg=$sql->borrar_todo();// me cargo toda la agenda
		if ($msg==true){
			echo "<h4>Agenda Borrada con éxito</h4>";
			?>
			<div class="mensajes">
				<br>
				<a href='añadir.php'>Ok, voy a añadir contactos</a>
				<a href='index.php'>Ok, conforme</a>
			</div>
			<?php
		}else{
			echo "<h4>Error al borrar la agenda :(</h4";
		}
	}else{
		$cue->borrar_todo();
		
	}
	
	$pie = new pie();
	$pie->mostrarPie();
	// cierro el contenedor y el html
	$p->htmlfin();
?>