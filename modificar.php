<?php
	class pagina{
		public function encabezado($title=""){
			$estilo1="css/estilo1.css";
			echo "<!DOCTYPE html>
	
	<head>
	    <meta charset='utf-8'>
	    <title>$title</title>
		<link href='".$estilo1."' type='text/css' rel='stylesheet'>       
	</head>
	<body>
	";
		}
		
		public function contenido(){
			echo "<div class='contenido'>";
			include("inc/cabecera.php"); 
			include("inc/cuerpo.php"); 
			include("inc/pie.php");
			
		}
		
		public function htmlfin(){
			echo "</div></body></html>";
		}
		
	}
	//compruebo que exista la base de datos
	if (!file_exists("contactos.db")){
		header("Location: index.php");//sino redirijo al inicio para crearla		
	}
	$element_menu=array("Inicio","Añadir","Listar","Modificar","Borrar","Borrar todo","Buscar");
	// creo la pagina
	$p = new pagina();
	
	// creo el head con el titulo
	$p->encabezado("Agenda : Modificar");
	
	// empiezo a añadir contenido
	$p->contenido();
	
	// creo la imagen y el menu de la cabecera
	$cab = new cabecera();
	$cab->banner();
	$cab->menu($element_menu);
	
	
	$cue = new cuerpo();
	$cue->title_modificar();
	include("inc/sql.php");
	$sql = new modelos();
	
	if(isset($_POST['modificar'])&&isset($_POST['id'])){
		// si recibo el usuario y he pulsado modificar, saco formulario de edicion del contacto
		$usuario=$sql->buscar_id($_POST['id']);
		$cue->formulario_añadir("modificar",$usuario);
		
	}else if(isset($_POST['Aceptar'])&&isset($_POST['id'])){
		
			$usuario=$sql->buscar_id($_POST['id']);
			$cue->formulario_añadir("modificar",$usuario);
		//si he seleccionado un contacto
		if(isset($_POST['nombre'])&&isset($_POST['telefono'])&&$_POST['nombre']!=""&&$_POST['telefono']!=""){
			//compruebo que tengo un nombre y un telefono en el formulario que añadir
			$existe_nombre=$sql->buscar_nombre($_POST['nombre'],$_POST['id']);// compruebo que nadie mas tenga el nombre modificado
			if($existe_nombre){//recibo true si puedo usar el nombre elegido
				if(is_numeric($_POST['telefono'])){//compruebo que sean numeros en el telefono
					$msg=$sql->insertar_datos($_POST,"modificar");
					if($msg==2){
						echo "<h3>Éxito al modificar</h3>";
						?>
						<div class="mensajes">
							<br>
							<a href='modificar.php'>Ok, quiero modificar otros</a>
							<a href='index.php'>Ok, suficiente</a>
						</div>
						<?php
					}else{
						echo "<h3>Error al modificar</h3>";
						echo "<a href='modificar.php'>Intentar de nuevo</a>";
					}
				}else{
					echo "<h3>El número de telefono sólo debe contener dígitos</h3>";
				}
			}else{
				echo "<h3>Ese contacto ya existe, introduce otro nombre</h3>";
			}
			
		}else{
			echo "<h3>Introduce al menos el nombre y el teléfono</h3>";
		}
		
	}else{// si acabo de entrar en la página listo toda la agenda
		$datos=$sql->listar();
		if ($datos!=null){// y puedo ordenarlos
			if((isset($_GET['c'])) && (isset($_GET['o']))){
				$campo=$_GET['c'];
				$orden=$_GET['o'];
				//vuelvo a pedir la consulta pero ahora ordenada
				$datos=$sql->listar($campo,$orden);
			}
			$cue->mostrar_datos($datos,"modificar");
			if(isset($_POST['modificar'])&&!isset($_POST['id'])){
				echo "<h3>Selecciona un contacto para modificar</h3>";
			}
		}else{
			echo "<h2>Tu lista de contactos está vacia :(</h2>";
		}
	}
	$pie = new pie();
	$pie->mostrarPie();
	// cierro el contenedor y el html
	$p->htmlfin();
?>
