<?php
	class modelos{
		
		public function crear_tabla(){
			try{
				$conn = new PDO ('sqlite:contactos.db');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
			
				$crear_tabla = "CREATE TABLE IF NOT EXISTS contactos(
					ID INTEGER PRIMARY KEY AUTOINCREMENT ,
					nombre TEXT,
					apellidos TEXT,
					email TEXT,
					telefono TEXT
				)";
				
				$conn->exec($crear_tabla);//ejecuta una sentencia
				
			}catch(PDOException $e){
				echo $e->getMessage();
			}
			$conn = null;
		}
		public function insertar_datos($contactos,$accion=""){
			
			//Una vez recibo los datos los convierto todos a minúsculas y luego pongo la primera mayúscula
			foreach ($contactos as $key => $value) {
				$contactos[$key]=ucfirst(strtolower($value));
			}
			try{	
				$conn = new PDO ('sqlite:contactos.db');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
				
				if ($accion=="añadir"){//si lo que quiero es añadir un contacto nuevo
						
					$insertar = "INSERT into contactos(nombre,apellidos,email,telefono)
					VALUES(:nombre, :apellidos, :email, :telefono)";
					
					//proteccion contra inyeccion de ataques sql
					$sentencia = $conn->prepare($insertar);
					$sentencia->execute(array_slice($contactos,0,-1));
					
					$msg="1";
				}else if($accion=="modificar"){// si quiero modificar un contacto
					$actualizar = "UPDATE contactos SET 
							nombre= :nombre, apellidos= :apellidos, email= :email, telefono= :telefono
				 			where id = :id";
					//proteccion contra inyeccion de ataques sql
					$sentencia = $conn->prepare($actualizar);
					$count=$sentencia->execute(array_slice($contactos,0,-1));
					
					$msg="2";
				}
			}catch(PDOException $e){
				return $e->getMessage();
			}
			$conn = null;
			return $msg;				
			
		}
		public function listar($campo="",$orden=""){
			try{//para mostrar los contactos y/o mostrarlos ordenados
				$conn = new PDO ('sqlite:contactos.db');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
				
				$listado = "SELECT * from contactos";//concateno la sentencia segun el criterio indicado
				if(isset($campo)&&$campo!=""&& isset($orden)){
					switch($campo){
						case "nombre":
							$listado = $listado." order by nombre";
							break;
						case "apellidos":
							$listado = $listado." order by apellidos";
							break;
						case "email":
							$listado = $listado." order by email";
							break;
						case "telefono":
							$listado = $listado." order by telefono";
							break;
					}	
					if(strtoupper($orden)=="ASC"){
						$listado=$listado." ASC";
					}elseif (strtoupper($orden)=="DESC") {
						$listado=$listado." DESC";
					}
				}
				
				$resultado = $conn->query($listado);
				//compruebo que la agenda tenga contactos que mostrar
				$resultado = $resultado->fetchAll();
				if(count($resultado)!=0){
					return $resultado;
				}else{
					return null;
				}
			}catch(PDOException $e){
				echo $e->getMessage();
			}
			$conn = null;
		}
		
		public function buscar($campo="",$criterio="",$columna="",$orden=""){
			try{// buscar registros con un criterio
				$conn = new PDO ('sqlite:contactos.db');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
					
				
				// según el campo seleccionado preparo un array con el valor
				switch($campo){
					case "nombre":
						$listado = "SELECT * from contactos where nombre like :nombre";
						$dato = array('nombre' => "%$criterio%" );
						break;
					case "apellidos":
						$listado = "SELECT * from contactos where apellidos like :apellidos";
						$dato = array('apellidos' => "%$criterio%" );
						break;
					case "email":
						$listado = "SELECT * from contactos where email like :email";
						$dato = array('email' => "%$criterio%" );
						break;
					case "telefono":
						$listado = "SELECT * from contactos where telefono like :telefono";
						$dato = array('telefono' => "%$criterio%" );
						break;
				}
				
				//segun la preferencia de orden añado un order by a la consulta
				if(isset($columna)&isset($orden)&$columna!=""&$orden!=""){
					switch($columna){
						case "nombre":
							$listado=$listado." order by nombre";
							break;
						case "apellidos":
							$listado=$listado." order by apellidos";
							break;
						case "email":
							$listado=$listado." order by email";
							break;
						case "telefono":
							$listado=$listado." order by telefono";
							break;
					}
					if(strtoupper($orden)=="ASC"){
						$listado=$listado." ASC";
					}elseif (strtoupper($orden)=="DESC") {
						$listado=$listado." DESC";
					}
				}
				//preparo la consulta
				$consulta = $conn->prepare($listado, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$consulta->execute($dato);// ejecuto la consulta con el parametro id
				$resultado = $consulta->fetchAll();//recojo resultados
				if(count($resultado)!=0){
					return $resultado;
				}else{
					return null;
				}
			}catch(PDOException $e){
				$conn = null;
				return $e->getMessage();
			}
			$conn = null;
			return $resultado;
		}
		
		
		public function buscar_id($id){
			try{
				$conn = new PDO ('sqlite:contactos.db');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
				$listado = "SELECT * from contactos where id = :id";// escribo la consulta
				$id= array('id' => $id );// preparo un array con el id del contacto
				
				//preparo la consulta
				$consulta = $conn->prepare($listado, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$consulta->execute($id);// ejecuto la consulta con el parametro id
				$resultado = $consulta->fetchAll();//recojo resultados
				//compruebo que la agenda tenga contactos que mostrar	
				if(count($resultado)!=0){
					return $resultado;
				}else{
					return null;
				}
			}catch(PDOException $e){
				$conn = null;
				return $e->getMessage();
			}
			$conn = null;
			return $resultado;
		}
		public function buscar_nombre($nombre,$id=""){
			try{
				$conn = new PDO ('sqlite:contactos.db');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
				if($id==""){//para añadir contacto, compburebo el no tener nombres repetidos
					$busqueda = "SELECT * from contactos where nombre like :nombre";
					$datos= array('nombre' => $nombre );// preparo un array con el id del contacto
					
				}else{/*para modificar contacto, tengo que comparar el contacto que modifico con los que ya hay, 
				 * excluyendo el mismo propio, porque en el caso de cambiar el telefono de mi contacto y el nombre lo dejo igual
				 * comparará el nombre consigo mismo y creerá que el nombre ya existe.
				*/
					$busqueda = "SELECT * from contactos where nombre like :nombre and id <> :id";// escribo la consulta
					$datos= array('nombre' => $nombre,'id' => $id );// preparo un array con el id del contacto
					
				}
				//preparo la consulta
				$consulta = $conn->prepare($busqueda, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$consulta->execute($datos);// ejecuto la consulta con el parametro nombre
				$resultado = $consulta->fetchAll();//recojo resultados
				
				//si encuentra alguno es que ya existe el contacto
				if(count($resultado)!=0){
					return false;
				}else{
					return true;
				}
			}catch(PDOException $e){
				$conn = null;
				return $e->getMessage();
			}
			$conn = null;
			return $resultado;
		}
		
		public function borrar_contacto($id_contacto){
			try{
				$conn = new PDO ('sqlite:contactos.db');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
				$borrado = "DELETE from contactos where id = :id";
				$id_contacto= array('id' => $id_contacto );
				//proteccion contra inyeccion de ataques sql
				$sentencia = $conn->prepare($borrado);
				$sentencia->execute($id_contacto);
				
			}catch(PDOException $e){
				$conn = null;
				return $e->getMessage();
			}
			$conn = null;
			return true;
		}
		
		public function borrar_todo(){
			try{
				$conn = new PDO ('sqlite:contactos.db');
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //muestra errores de la conexion
				$borrado = 'DELETE from contactos';
				$conn->exec($borrado);
			}catch(PDOException $e){
				$conn = null;
				return $e->getMessage();
			}
			$conn = null;
			return true;
		}
	}
?>