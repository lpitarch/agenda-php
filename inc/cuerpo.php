<?php
	class cuerpo{
		/* creo metodos para mostrar titulos*/
		public function title_inicio(){
			?>
			<h1>Clic en el menú para usar la agenda! >:o</h1>
			<?php
		}
		public function title_añadir(){
			?>
			<h1>Añadir contacto</h1>
			<?php
		}
		public function title_buscar(){
			?>
			<h1>Buscar contacto</h1>
			<?php
		}
		public function title_modificar(){
			?>
			<h1>Modificar contacto</h1>
			<?php
		}
		public function title_borrar_contacto(){
			?>
			<h1>Borrar contacto</h1>
			<?php
		}
		public function title_listar(){
			?>
			<h1>Lista de contactos </h1>
			<?php
		}
		public function title_borrar(){
			?>
			<h1>Borrar Agenda </h1>
			<?php
		}
		public function portada(){
			?>
			<img src="img/monkey.png" alt="mono portada" />
			<?php
		}
		public function borrar_todo(){
			/* formulario para borrar toda la agenda*/
			?>
			<h2>¿Estás seguro que quieres borrar todo?</h2>
			<form action='borrar_todo.php' method='post'>
				<input type='submit' value='Si' name='borrar'>
			</form>
			<form action='index.php' method='post'>
				<input type='submit' value='No'>
			</form>
			<?php
		}
		public function form_buscar(){
			/* formulario para buscar por la agenda*/
			?>
				<form action="buscar.php" method="get">
					<table>
						<tr>
							<td><label for="campo"> Criterio: </label></td>
							<td><select name="campo" id="campo">
							  <option value="nombre">Nombre</option>
							  <option value="apellidos">Apellidos</option>
							  <option value="email">Email</option>
							  <option value="telefono">Telefono</option>
							</select>
							</td>
						</tr>
						<tr>
							<td><label for="criterio">Texto a buscar: </label></td>
							<td><input type="text" name="criterio" id="criterio" /></td>
						</tr>
						<tr>
							<td colspan="2">
							<input type="submit" value="Buscar"/>
							</td>
						</tr>
					</table>
				</form>
				
			<?php
		}		
		public function form_borrar_contacto(){
			/* formulario para borrar un contacto en concreto*/
			
			if(isset($_POST["nombre"])){
			?>
			
			<h2>¿Estás seguro que quieres borrar al usuario <?php echo $_POST["nombre"]?> ?</h2>
			<form action='borrar_usuario.php' method='post'>
				<input type="hidden" value="<?php echo $_POST["id"]?>" name="id">
				<input type='submit' value='Si' name='borrar'>
			</form>
			<form action='index.php' method='post'>
				<input type='submit' value='No'>
			</form>
			<?php
			}
			?>
			
			
			<?php
		}
		
		public function mostrar_datos($resultado,$accion=""){
			/* tabla para mostrar los contactos */
			if(isset($_GET['campo'])&&$_GET['campo']!=""){// si pido ordenarlo me guardo lo que viene de get en una busqueda para luego añadirlos a los enlaces
				$busqueda="campo=".$_GET['campo']."&criterio=".$_GET['criterio']."&";
			}
			?>
			<table class='lista'>
					<tr class="cabecera">
						<td><?php
						//si se pide ordenar por get comparo el campo seleccionado
							if(isset($_GET['c'])&&$_GET['c']=='nombre') {
								if($_GET['o']=='DESC'){ //si paso por get el orden pongo un enlace que lleve a mostrar el orden contrario
									?>
									<a href="?
									<?php if(isset($busqueda)){echo $busqueda;}?>
										c=nombre&o=ASC"><strong>Nombre</strong></a>
									<?php
								}else{
									?>
									<a href="?
									<?php if(isset($busqueda)){echo $busqueda;}?>
									c=nombre&o=DESC"><strong>Nombre</strong></a>
									<?php
								}
							}else{// si no tengo nada seleccionado muestro un enlace para poder ordenarlo por ese campo
								?>
								<a href="?
								<?php if(isset($busqueda)){echo $busqueda;}?>
								c=nombre&o=DESC">Nombre</a>
								<?php	
							}
							?>
						</td>
						
						<td><?php
						//si se pide ordenar por get comparo el campo seleccionado
							if(isset($_GET['c'])&&$_GET['c']=='apellidos') {
								if($_GET['o']=='DESC'){ //si paso por get el orden pongo un enlace que lleve a mostrar el orden contrario
									?>
									<a href="?<?php if(isset($busqueda)){echo $busqueda;}?>c=apellidos&o=ASC"><strong>Apellidos</strong></a>
									<?php
								}else{
									?>
									<a href="?<?php if(isset($busqueda)){echo $busqueda;}?>c=apellidos&o=DESC"><strong>Apellidos</strong></a>
									<?php
								}
							}else{// si no tengo nada seleccionado muestro un enlace para poder ordenarlo por ese campo
								?>
								<a href="?<?php if(isset($busqueda)){echo $busqueda;}?>c=apellidos&o=DESC">Apellidos</a>
								<?php	
							}
							?>
						</td>
						
						<td><?php
						//si se pide ordenar por get comparo el campo seleccionado
							if(isset($_GET['c'])&&$_GET['c']=='email') {
								if($_GET['o']=='DESC'){ //si paso por get el orden pongo un enlace que lleve a mostrar el orden contrario
									?>
									<a href="?<?php if(isset($busqueda)){echo $busqueda;}?>c=email&o=ASC"><strong>Email</strong></a>
									<?php
								}else{
									?>
									<a href="?<?php if(isset($busqueda)){echo $busqueda;}?>c=email&o=DESC"><strong>Email</strong></a>
									<?php
								}
							}else{// si no tengo nada seleccionado muestro un enlace para poder ordenarlo por ese campo
								?>
								<a href="?<?php if(isset($busqueda)){echo $busqueda;}?>c=email&o=DESC">Email</a>
								<?php	
							}
							?>
						</td>
						<td><?php
						//si se pide ordenar por get comparo el campo seleccionado
							if(isset($_GET['c'])&&$_GET['c']=='telefono') {
								if($_GET['o']=='DESC'){ //si paso por get el orden pongo un enlace que lleve a mostrar el orden contrario
									?>
									<a href="?<?php if(isset($busqueda)){echo $busqueda;}?>c=telefono&o=ASC"><strong>Telefono</strong></a>
									<?php
								}else{
									?>
									<a href="?<?php if(isset($busqueda)){echo $busqueda;}?>c=telefono&o=DESC"><strong>Telefono</strong></a>
									<?php
								}
							}else{// si no tengo nada seleccionado muestro un enlace para poder ordenarlo por ese campo
								?>
								<a href="?<?php if(isset($busqueda)){echo $busqueda;}?>c=telefono&o=DESC">Telefono</a>
								<?php	
							}
							?>
						</td>
					<?php
					/* segun la acción indico una cabecera y un destino de formulario distinto*/
					if ($accion=="modificar"){
						?>
						<form method="post" action="modificar.php">
						<td colspan="2">Seleccione un contacto </td>
						<?php
						
					}else if($accion=="borrar"){
						?>
						<form method="post" action="borrar.php">
						<td colspan="2">Seleccione uno o varios contactos</td>
						<?php
					}else{
						?>
						<td colspan="2">Opciones</td>
						<?php
					}
					?>
					</tr>
					
					<?php
					$c=0;
					foreach($resultado as $emp){/* recorro los datos recibidos para ir mostrándolos */
						?>
						<tr <?php
							if(($c % 2)==0){/* doy una clase cada fila par o impar*/
								echo "class='par'";
							}else{
								echo "class='impar'";
							}
							$c++;
						/* creo label para poder clicar en las filas y seleccionar facilmente los input type radio y checkbox*/
						?>
						><td><label for="<?php echo $emp['ID'] ?>"><?php echo $emp['nombre'] ?></label></td>
							<td><label for="<?php echo $emp['ID'] ?>"><?php echo $emp['apellidos'] ?></label></td>
							<td><label for="<?php echo $emp['ID'] ?>"><?php echo $emp['email'] ?></label></td>
							<td><label for="<?php echo $emp['ID'] ?>"><?php echo $emp['telefono'] ?></label></td>
							
							
							<?php
						if ($accion=="modificar"){/* si modifico pongo input radio */
							?>
							<td>
								<label for="<?php echo $emp['ID'] ?>">
								<input type="radio" name="id" value="<?php echo $emp['ID'] ?>" id="<?php echo $emp['ID'] ?>"></label></td>
							
							<?php
							
						}else if($accion=="borrar"){/* si borro pongo checkbox */
							?>
							<td>
								<label for="<?php echo $emp['ID'] ?>">
								<input type="checkbox" name="id[]" value="<?php echo $emp['ID'] ?>" id="<?php echo $emp['ID'] ?>"></label></td>
							<?php
						}else{/* si no, es una lista normal y pongo dos botones para mayor funcionalidad */
							?>
							<td>
									<form action="modificar.php" method="post">
										<input type="hidden" name="nombre" value="<?php echo $emp['nombre'] ?>">
										<input type="hidden" name="id" value="<?php echo $emp['ID'] ?>">
										<input type="submit" value="Editar" name="modificar">
									</form>
									
								</td>
								<td>
									<form action="borrar_usuario.php" method="post">
										<input type="hidden" name="nombre" value="<?php echo $emp['nombre'] ?>">
										<input type="hidden" name="id" value="<?php echo $emp['ID'] ?>">
										<input type="submit" value="Borrar">
									</form>
								</td>
								
							<?php
						}
						?>
						</tr>
						<tr>
						<?php		
					}
					if ($accion=="modificar"){
						?>
						<td><input type="submit" value="Modificar" name="modificar"></td>
						</form>
						<?php
						}else if($accion=="borrar"){
							?>
							<td><input type="submit" value="Borrar" name="borrar"></td>
						</form>
							<?php
						}
					?>
					</tr>
					</table>
					<?php
		}
		public function formulario_añadir($accion="",$datos=""){
			if($accion=="añadir"){ 
			 /* si quiero añadir muestro el formulario y si hay algun campo erróneo al enviar, guardo los campos
			  * escritos con anterioridad */
				?>
				<form action='añadir.php' method='post' novalidate  id="miform">
					<table class='form_añadir'>
						<tr>
							<td><label for='nombre'>Nombre: *</td>
							<td><input type='text' name='nombre' id='nombre' 
								<?php if(isset($_POST['nombre'])) {echo "value='".$_POST['nombre']."'";}?>/></label></td>
						</tr>
						<tr>
							<td><label for='apellidos'>Apellidos: </td>
							<td><input type='text' name='apellidos' id='apellidos' 
								<?php if(isset($_POST['apellidos'])) {echo "value='".$_POST['apellidos']."'";}?>/></label></td>
						</tr>
						<tr>
							<td><label for='email'>Email: </td>
							<td><input type='email' name='email' id='email' 
								<?php if(isset($_POST['email'])) {echo "value='".$_POST['email']."'";}?>/></label></td>
						</tr>
						<tr>
							<td><label for='telefono'>Telefono: * </td>
							<td><input type='text' name='telefono' id='telefono' 
								<?php if(isset($_POST['telefono'])) {echo "value='".$_POST['telefono']."'";}?>/></label></td>
						</tr>
						<tr>
							<td>
								<input type='submit' value='Añadir' name='enviar'>
								<input type='reset' value='Reiniciar' onClick="vaciarCampos()" >
							</td>
						</tr>
					</table>
					
				</form>
				
				<?php
			}else if($accion="modificar"){
				$datos = $datos[0];
				/* si modifico, muestro el formulario con los datos de la base de datos */
				?>
					<form action='modificar.php' method='post' novalidate>
						<table class='form_añadir'>
							<tr>
								<td><label for='nombre'>Nombre: *</td>
								<td><input type='text' name='nombre' id='nombre' value="<?php echo $datos['nombre']?>"/></label></td>
							</tr>
							<tr>
								<td><label for='apellidos'>Apellidos: </td>
								<td><input type='text' name='apellidos' id='apellidos' value="<?php echo $datos['apellidos']?>"/></label></td>
							</tr>
							<tr>
								<td><label for='email'>Email: </td>
								<td><input type='email' name='email' id='email' value="<?php echo $datos['email']?>"/></label></td>
							</tr>
							<tr>
								<td><label for='telefono'>Telefono: * </td>
								<td><input type='text' name='telefono' id='telefono' value="<?php echo $datos['telefono']?>"/></label></td>
							</tr>
							<tr>
							
								<td>
									<input type="hidden" name="id" value="<?php echo $datos['ID']?>"/>
									<input type='submit' value='Aceptar' name='Aceptar'/>
									<input type='reset' value='Reiniciar' onClick="vaciarCampos()" ></td>
							</tr>
						</table>
					</form>
				<?php
			}/* para que el reset funcione en html5, me ayudo de javascript */
			?>
			<script>
					function vaciarCampos(){
					       document.getElementById("nombre").setAttribute("value","");
					       document.getElementById("apellidos").setAttribute("value","");
					       document.getElementById("email").setAttribute("value","");
					       document.getElementById("telefono").setAttribute("value","");
					}
				</script>
			<?php
		}
	}
?>